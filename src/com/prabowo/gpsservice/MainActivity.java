package com.prabowo.gpsservice;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.view.View;

public class MainActivity extends Activity implements BroadcastReceiver{
	
	private Button openService;
	private EditText serviceIp;
	private LinearLayout groupOpenService;
	
	private Button closeService;
	private LinearLayout groupCloseService;
	
	@Override
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		
		final Intent service = new Intent(getBaseContext(),MainService.class);
		
		setContentView(R.layout.main);
		
		this.openService = (Button) findViewById(R.id.openService);
		this.serviceIp = (EditText) findViewById(R.id.serverIp);
		this.groupOpenService = (LinearLayout) findViewById(R.id.groupOpenService);
		
		this.closeService = (Button) findViewById(R.id.closeService);
		this.groupCloseService = (LinearLayout) findViewById(R.id.groupCloseService);
		
		this.openService.setOnClickListener(new Button.OnClickListener(){
			
			@Override
			public void onClick(View v){
				
				// Globals.setIp(serviceIp.getText());
				
				startService(service);
				reShow();
			}
		});
		
		this.closeService.setOnClickListener(new Button.OnClickListener(){
			
			@Override
			public void onClick(View v){
				stopService(service);
				reShow();
			}
		});
		
		reShow();
	}
	
	
	private void reShow(){
		if(isMyServiceRunning(MainService.class)){
			this.groupCloseService.setVisibility(View.VISIBLE);
			this.groupOpenService.setVisibility(View.GONE);
		}else{
			this.groupCloseService.setVisibility(View.GONE);
			this.groupOpenService.setVisibility(View.VISIBLE);
		}
	}
	
	
	private boolean isMyServiceRunning(Class<?> serviceClass) {
		
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
