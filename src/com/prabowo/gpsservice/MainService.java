package com.prabowo.gpsservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Bundle;
import android.content.Context;
import android.util.Log;


import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;

import java.net.Socket;


public class MainService extends Service{
	
	private final String TAG = "GPSService";
	private LocationManager lManager;
	private LocationListener listener;
	private Context context = null;
	
	@Override
	public IBinder onBind(Intent intent){
		return null;
	}
	
	void getLocation(){
		Log.d(TAG,"Looking Location");
		
		lManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
		listener = new LocationHandler();
		lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,10,listener);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags,int startId){
		super.onStartCommand(intent,flags,startId);
		Log.d(TAG,"Service Running");
		
		this.context = getApplicationContext();
		
		
		getLocation();
		return Service.START_STICKY;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		lManager.removeUpdates(listener);
		lManager = null;
		Log.d(TAG,"SERVICE DESTROYED");
	}
	
	private class LocationHandler implements LocationListener{
		
		@Override
		public void onLocationChanged(Location loc){
			Log.d(TAG,"Latitude => " + loc.getLatitude());
		}
		
		@Override
		public void onProviderDisabled(String provider){}
		
		@Override
		public void onProviderEnabled(String provider){}
		
		@Override
		public void onStatusChanged(String provider,int status, Bundle extras){
			System.out.println(TAG + status);
		}
	}
}
